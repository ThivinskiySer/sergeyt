$(document).ready(function(){
    $("#registrationForm").validate({
        rules:{
            firstName:{
                required: false,
                 minlength: 2

            },
            lastName:{
                required: false,
                minlength: 2
            },
            email:{
                required: false,
                email: true
            },
            nickName:{
                required: false,
                maxlength: 20,
                minlength: 3

            },
            password:{
                required: false,
                maxlength: 16,
                minlength: 6
            },

            password_again: {
                equalTo: "#password"
            },

            phone:{
                required: false,
                phoneUKR: true
            }
        }
    });

});
