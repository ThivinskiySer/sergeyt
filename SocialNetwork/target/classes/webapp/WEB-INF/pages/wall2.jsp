<%--

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#avatar").error(function () {
                if ($("#gender").text() == "male") {
                    $(this).attr("src", "../images/kenny.jpg");
                } else {
                    $(this).attr("src", "../images/princess_kenny.jpg");
                }
            });

            $("#photo").change(function () {
                $("#file_name").val($("#photo").val().replace(/C:\\fakepath\\/i, ''));
                $("#upload_photo_form").submit();
            });

        });
    </script>
    <title></title>
    <h1>WALL</h1>
</head>
<body>

<div style="width: 250px; height: 250px">
    <p id="gender">${gender}</p>
    <img id="avatar" class="img-thumbnail" src="/avatar.jpeg" style="width: 250px; height: 250px"/>

    <form id="upload_photo_form" method="post" enctype="multipart/form-data" action="/upload">
        <button type="button" class="btn btn-sm btn-default" onclick="$(this).next().click()">Замінити
            фото
        </button>
        <input type="file" name="file" id="photo" style="visibility: hidden; position: absolute;">
        <input id="file_name" type="hidden" name="name"><br/>
        <input type="hidden" name="nickName"
               value="<sec:authentication property="principal.username" />"><br/>
    </form>
</div>
</body>
</html>
