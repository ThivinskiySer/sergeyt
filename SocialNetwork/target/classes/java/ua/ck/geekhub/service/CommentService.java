package ua.ck.geekhub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.geekhub.dao.CommentDAO;
import ua.ck.geekhub.domain.Record;

import javax.transaction.Transactional;

@Service
@Transactional
public class CommentService {

    @Autowired
    private CommentDAO commentDAO;

    public void saveComment(String content, Record record) {
        commentDAO.saveComment(content, record);
    }
}
