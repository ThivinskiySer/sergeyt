package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.ck.geekhub.service.ImageService;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.security.Principal;


@Controller
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class FileUploadController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "/avatar.jpeg", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public
    @ResponseBody
    byte[] provideUploadInfo(Principal principal) {

        return imageService.getUserAvatar(principal.getName()).getData();
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String handleFileUpload(
            @RequestParam String name,
            @RequestParam MultipartFile file,
            @RequestParam String nickName,
            ModelMap modelMap
    ) {
        if (!file.isEmpty()) {
            try {
                imageService.saveImage(name, file.getBytes(), nickName);
            } catch (IOException e) {
                modelMap.addAttribute("errorMessage", "Error uploading file!");
                return "error";
            }
        }
        return "redirect:personalPage";

    }


}
