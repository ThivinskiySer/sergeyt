package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.service.UserService;

import java.security.Principal;

@Controller
public class UsersController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/", method = {RequestMethod.GET})
    public String printWelcome() {
        return "index";
    }

    @RequestMapping(value = "/personalPage", method = RequestMethod.GET)
    public String personalPage(Principal principal, ModelMap map) {
        Integer id = userService.getUserByNickName(principal.getName()).getId();
        return "redirect:/id" + id;
    }

    @RequestMapping(value = "/id{id}", method = RequestMethod.GET)
    public String personalPage2(@PathVariable("id") Integer userID, Principal principal, ModelMap map) {
        if (userService.getUser(userID) == null) {
            return "error";
        }
        map.addAttribute("gender", userService.getUser(userID).getGender());
        map.addAttribute("userID", userID);
        map.addAttribute("records", userService.getWallRecords(userID));
        if (principal.getName().equals(userService.getUser(userID).getNickName())) {
            return "personalPage";
        } else {
            return "user_wall";
        }
    }

    @RequestMapping("/wall/{id}")
    public String wall(@PathVariable("id") Integer userID, ModelMap map) {
        System.out.println("++++++++++++++" + userID);
        map.addAttribute("userID", userID);
        map.addAttribute("records", userService.getWallRecords(userID));
        return "wall";
    }

    @RequestMapping(value = "/registration", method = {RequestMethod.GET, RequestMethod.POST})
    public String registrationNewUser() {
        return "registration";
    }

    @RequestMapping(value = "/registrationCheck", method = {RequestMethod.POST})
    public String createUser(
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email,
            @RequestParam String nickName,
            @RequestParam String password,
            @RequestParam String phone,
            @RequestParam String birthday,
            @RequestParam String gender

    ) {
        userService.createUser(firstName, lastName, email, nickName, password, phone, birthday, gender);
        return "redirect:registrationComplete";
    }

    @RequestMapping(value = "/authorization", method = {RequestMethod.GET})
    public String authorizationUser() {
        return "authorization";
    }

    @RequestMapping(value = "/registrationComplete", method = {RequestMethod.GET})
    public String registrationComplete() {
        return "registrationComplete";
    }


}