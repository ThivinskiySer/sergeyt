<%--
<form action="/createRecord" method="post">
<input type="hidden" name="userID" value="${userID}">
<input type="text" name="recordContent" >
<input type="hidden" name="authorUserName"
   value="<sec:authentication property="principal.username"/>"><br/>
<input type="submit" value="Відправити">
</form>

<form action="/createComment" method="post">
<input type="hidden" name="userID" value="${userID}"><br/>
<input type="hidden" id="recordID" name="recordID" ><br/>
<input type="hidden" id="commentContentID" name="commentContent" ><br/>

<c:forEach var="record" items="${records}">
<div class="comments">
    <input class="recordID" type="hidden" value="${record.id}">
    Record <c:out value="${record.content}"/><br/>
    <c:out value="${record.author}"/><br/>
    <c:forEach var="comment" items="${record.comments}">
    Comments <c:out value="${comment.content}"/><br/>
    </c:forEach>
    <input type="text" class="comment"  placeholder="Коментар">
</div>
<input class="submit" type="submit" onclick="comment()" value="Прокоментувати"><br/>
</c:forEach>
</form>

data : ({
            commentContent: commentContent,
            userID: userID,
            recordID: recordID
        }),
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>

    <title></title>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript">
        <%--
    --%>
        $(document).ready(function () {
            $(".submit").click(function comment() {

                var commentContent = $(this).prev(".comments").find(".comment").val();
                var recordID = $(this).prev(".comments").find(".recordID").val();
                var userID = "${userID}";

                $.ajax({
                    url: 'createComment',
                    type: 'GET',
                    dataType: 'json',

                    data: {
                        commentContent: commentContent,
                        userID: userID,
                        recordID: recordID
                    },

                    success: function (data) {
                        alert("GOOD");
                        console.log(data);
                        $("#comment").val(commentContent);
                        $.each(data, function (k, v) {
                            console.log(v.recordContent);
                            $.each(v.commentsList, function (k, v) {
                                console.log(v);
                                $(".par").html(v);
                            })
                        })
                    }


                });

            });
        })
        ;
    </script>
</head>
<body>


<form action="/createRecord" method="post">
    <input type="hidden" name="userID" value="${userID}">
    <input type="text" name="recordContent">
    <input type="hidden" name="authorUserName"
           value="<sec:authentication property="principal.username"/>"><br/>
    <input type="submit" value="Відправити">
</form>


<c:forEach var="record" items="${records}">
    <div>
        <div class="comments">
            <input class="recordID" type="hidden" value="${record.id}">
            Record <c:out value="${record.content}"/><br/>
            <c:out value="${record.author}"/><br/>
            <c:forEach var="comment" items="${record.comments}">
                Comments <c:out value="${comment.content}"/><br/>
            </c:forEach>
            <input type="text" id="comment"><br/>
            <input type="text" class="comment" placeholder="Коментар"><br/>
        </div>
        <input class="submit" type="button" value="Прокоментувати"><br/>
    </div>
</c:forEach>
<p class="par"></p>
</body>
</html>
