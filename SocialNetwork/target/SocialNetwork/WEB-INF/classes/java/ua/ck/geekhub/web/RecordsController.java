package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.service.RecordService;
import ua.ck.geekhub.service.UserService;

@Controller
public class RecordsController {

   @Autowired
   private RecordService recordService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/createRecord", method = {RequestMethod.POST})
    public String createRecord(
            @RequestParam String recordContent,
            @RequestParam String userID,
            @RequestParam String authorUserName
    ){
       recordService.createRecord(recordContent, userService.getUser(Integer.parseInt(userID)), authorUserName);
        return "redirect:/id"+userID;
    }
}
