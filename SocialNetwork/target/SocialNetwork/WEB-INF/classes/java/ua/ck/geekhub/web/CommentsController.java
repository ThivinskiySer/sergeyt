package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ua.ck.geekhub.POJO.RecordsComments;
import ua.ck.geekhub.domain.Comment;
import ua.ck.geekhub.domain.Record;
import ua.ck.geekhub.service.CommentService;
import ua.ck.geekhub.service.RecordService;
import ua.ck.geekhub.service.UserService;

import java.util.*;

@Controller
public class CommentsController {

    @Autowired
    private CommentService commentService;
    @Autowired
    private RecordService recordService;
    @Autowired
    UserService userService;

   @RequestMapping(value = "/createComment", method = RequestMethod.GET)
    public @ResponseBody List<RecordsComments> personalPage(
            @RequestParam String commentContent,
            @RequestParam Integer userID,
            @RequestParam Integer recordID
    ) {

        commentService.saveComment(commentContent, recordService.getRecord(recordID));
        List<RecordsComments> responseList = new ArrayList<>();
        for (Record record : userService.getWallRecords(userID)){
            RecordsComments recordsComments = new RecordsComments();
            recordsComments.setRecordContent(record.getContent());
            ArrayList <String> comments = new ArrayList<>();
            for (Comment comment : record.getComments()){
                comments.add(comment.getContent());
            }
            recordsComments.setCommentsList(comments);
            responseList.add(recordsComments);
        }
        return responseList;

    }

}
