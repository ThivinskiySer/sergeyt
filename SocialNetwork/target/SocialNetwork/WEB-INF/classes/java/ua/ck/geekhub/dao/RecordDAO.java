package ua.ck.geekhub.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.domain.Record;
import ua.ck.geekhub.domain.User;

@Repository
public class RecordDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void saveRecord(Record record){
        sessionFactory.getCurrentSession().saveOrUpdate(record);
    }

    public void createRecord(String content, User user, String author){
        Record record = new Record();
        record.setContent(content);
        record.setUser(user);
        record.setAuthor(author);
        saveRecord(record);
    }

    public Record getRecord(Integer id){
        return (Record) sessionFactory.getCurrentSession().get(Record.class, id);
    }

}
