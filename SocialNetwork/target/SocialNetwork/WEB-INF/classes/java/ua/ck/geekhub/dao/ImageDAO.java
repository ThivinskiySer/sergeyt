package ua.ck.geekhub.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.domain.Image;
import ua.ck.geekhub.service.UserService;

@Repository
public class ImageDAO {

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    UserService userService;

    public void saveImage(String name, byte[] imageData, String nickName){
        Image image = new Image();
        image.setImageName(name);
        image.setData(imageData);
        image.setUser(userService.getUserByNickName(nickName));
        sessionFactory.getCurrentSession().saveOrUpdate(image);
    }

    public Image getImage(Integer id){
        return (Image) sessionFactory.getCurrentSession().get(Image.class, id);
    }

    public Image getUserAvatar (String nickName) {
      return getImage(userService.getUserByNickName(nickName).getAvatar().getId());
    }

}
