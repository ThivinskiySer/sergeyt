$(document).ready(function(){
    $("#registrationForm").validate({
        rules:{
            firstName:{
                required: false

            },
            lastName:{
                required: false
            },
            email:{
                required: false,
                email: true
            },
            nickName:{
                required: false,
                maxlength: 16,
                minlength: 3

            },
            password:{
                required: false,
                maxlength: 16,
                minlength: 1
            },

            password_again: {
                equalTo: "#password"
            },

            phone:{
                required: false,
                phoneUKR: true
            }
        }
    });

});
