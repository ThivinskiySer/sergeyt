package ua.ck.geekhub.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.domain.Comment;
import ua.ck.geekhub.domain.Record;


@Repository
public class CommentDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void saveComment(String content, Record record) {
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setRecord(record);
        sessionFactory.getCurrentSession().saveOrUpdate(comment);
    }

    public Comment getComment(Integer id) {
        return (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
    }
}
