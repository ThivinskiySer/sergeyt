package lesson11.source;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
@Component
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    @Autowired
    public SourceLoader(@Qualifier("fileSourceProvider") SourceProvider fileSourceProvider,
                        @Qualifier("URLSourceProvider") SourceProvider urlSourceProvider) {

        sourceProviders.add(fileSourceProvider);
        sourceProviders.add(urlSourceProvider);
    }

    public String loadSource(String pathToSource) throws IOException {
        String result;
        if (Pattern.matches("^(ftp|https?://)?(www\\.)?([\\w\\.]+)\\.([a-z]{2,6}\\.?)(/.+)*/?$", pathToSource)) {
            result = sourceProviders.get(1).load(pathToSource);
        } else if (Pattern.matches("^([A-Z]:\\\\).+$", pathToSource)) {
            result = sourceProviders.get(0).load(pathToSource);
        } else {
            throw new IOException();
        }
        return result;
    }
}
