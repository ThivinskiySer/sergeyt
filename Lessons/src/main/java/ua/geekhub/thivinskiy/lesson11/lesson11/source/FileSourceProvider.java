package lesson11.source;

import org.springframework.stereotype.Component;

import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
@Component
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        return new File(pathToSource).exists();
    }

    @Override
    public String load(String pathToSource) throws IOException {
        String contents;
        StringBuilder result = new StringBuilder();
        if (isAllowed(pathToSource)) {
            try (BufferedReader in = new BufferedReader(new FileReader(pathToSource))) {
                while ((contents = in.readLine()) != null) {
                    result.append(contents);
                    result.append("\n");
                }
            }
            return result.toString();
        } else {
            throw new FileNotFoundException();
        }
    }
}
