package lesson11;

import lesson11.source.SourceLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Scanner;


public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SourceLoader sourceLoader = context.getBean(SourceLoader.class);
        Translator translator = context.getBean(Translator.class);

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) {
            //      So, the only way to stop the application is to do that manually or type "exit"
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);

                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);

                command = scanner.next();
            } catch (Exception e) {
                System.out.println("Enter another path to translation");
                command = scanner.next();
            }
        }

    }
}
