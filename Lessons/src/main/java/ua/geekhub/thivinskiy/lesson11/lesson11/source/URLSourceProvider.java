package lesson11.source;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
@Component
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            new URL(pathToSource).openStream();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws IOException {
        if (isAllowed(pathToSource)) {
            String contents;
            StringBuilder result = new StringBuilder();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL(pathToSource).openStream()))) {
                while ((contents = bufferedReader.readLine()) != null) {
                    result.append(contents);
                    result.append("\n");
                }
            }
            return result.toString();
        } else {
            throw new IOException();
        }
    }
}
