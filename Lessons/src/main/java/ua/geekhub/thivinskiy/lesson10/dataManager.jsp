<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title></title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>

    <script>
        function sendDelete(ths) {
            var tr = ths.parentNode.parentNode,
                    name = tr.getElementsByTagName("td")[0].innerHTML,
                    value = tr.getElementsByTagName("td")[1].innerHTML;
            document.getElementById("name").value = tr.getElementsByTagName("td")[0].innerHTML;
            document.getElementById("value").value = tr.getElementsByTagName("td")[1].innerHTML;
            document.getElementById("methodName").value = "delete";
            document.getElementById("formName").submit();
        }
        function sendPut() {
            document.getElementById("methodName").value = "put";
        }
    </script>

</head>
<body>
<form action="app" id="formName">
    <input type="hidden" id="name" name="name">
    <input type="hidden" id="value" name="value">
    <input type="hidden" id="methodName" name="methodName" value="get">
    <table style="width:500px">
        <tr>
            <th>Name</th>
            <th>Value</th>
            <th>Action</th>
        </tr>
        <c:forEach var="entity" items="${sessionScope.entities}">
            <tr>
                <td>${entity.name}</td>
                <td>${entity.value}</td>
                <td><a href="#" onclick="sendDelete(this)">Delete</a></td>
            </tr>
        </c:forEach>
        <td><input type="text" id="addName" name="addName"></td>
        <td><input type="text" id="addValue" name="addValue"></td>
        <td><input type="submit" value="add" onclick="sendPut()"></td>
    </table>
</form>
</body>
</html>
