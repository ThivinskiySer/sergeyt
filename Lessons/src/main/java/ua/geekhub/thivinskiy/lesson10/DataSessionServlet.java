package lesson10;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@WebServlet(name = "lesson10", urlPatterns = {"/app"})
public class DataSessionServlet extends HttpServlet {
    ArrayList entities;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getSession().getAttribute("entities") == null) {
            entities = new ArrayList();
            fillList(entities);
            Collections.sort(entities);
            req.getSession().setAttribute("entities", entities);
            req.getRequestDispatcher("dataManager.jsp").forward(req, resp);
        } else {
            Collections.sort((ArrayList) req.getSession().getAttribute("entities"));
            req.getRequestDispatcher("dataManager.jsp").forward(req, resp);
        }
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String methodName = "";

        if (req.getParameter("methodName") == null) {
            doGet(req, resp);
        } else {
            methodName = req.getParameter("methodName");
        }

        switch (methodName) {
            case "get":
                System.out.println("Service get");
                doGet(req, resp);
                break;
            case "delete":
                doDelete(req, resp);
                break;
            case "put":
                System.out.println("Service post");
                doPut(req, resp);
                break;
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        entities = (ArrayList) req.getSession().getAttribute("entities");
        String name = req.getParameter("name");
        String value = req.getParameter("value");
        Entity entity = new Entity(name, value);
        entities.remove(entity);
        doGet(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        entities = (ArrayList) req.getSession().getAttribute("entities");
        String name = req.getParameter("addName");
        String value = req.getParameter("addValue");
        Entity entity = new Entity(name, value);
        entities.add(entity);
        doGet(req, resp);
    }

    private void fillList(List list) {
        list.add(new Entity("Cat", "Tom"));
        list.add(new Entity("Cat", "Dave"));
        list.add(new Entity("Dog", "Bim"));
        list.add(new Entity("Mouse", "Jerry"));
    }
}
