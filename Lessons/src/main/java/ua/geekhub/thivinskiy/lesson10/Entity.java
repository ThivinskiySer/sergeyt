package lesson10;

public class Entity implements Comparable {
    private String name;
    private String value;

    public Entity(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entity entity = (Entity) o;

        if (name != null ? !name.equals(entity.name) : entity.name != null) return false;
        if (value != null ? !value.equals(entity.value) : entity.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        int result = this.name.compareTo(((Entity) o).getName());
        if (result == 0) {
            return this.value.compareTo(((Entity) o).getValue());
        } else {
            return result;
        }
    }
}
