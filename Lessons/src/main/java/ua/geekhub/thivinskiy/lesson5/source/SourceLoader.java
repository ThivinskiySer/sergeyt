package ua.geekhub.thivinskiy.lesson5.source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader() {
        sourceProviders.add(new FileSourceProvider());
        sourceProviders.add(new URLSourceProvider());
    }

    public String loadSource(String pathToSource) throws IOException {
        String result;
        if (Pattern.matches("^(ftp|https?://)?(www\\.)?([\\w\\.]+)\\.([a-z]{2,6}\\.?)(/.+)*/?$", pathToSource)){
            result = sourceProviders.get(1).load(pathToSource);
        } else if (Pattern.matches("^([A-Z]:\\\\).+$", pathToSource)) {
            result = sourceProviders.get(0).load(pathToSource);
        } else throw new IOException();
        return result;
    }
}
