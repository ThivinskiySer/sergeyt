<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <title></title>
</head>
<body>
<p id="selectedpath">${filepath}<p/>

<p id="previouspath">${previouspath}<p/>

<form method="get" action="app">
    <input type="hidden" id="methodName" name="methodName" value="get">
    <input type="hidden" id="filepath" name="filepath">
    <select id="fileList" size="5" name="files">
        <c:forEach var="fileName" items="${fileNamesArray}">
            <option>${fileName}</option>
        </c:forEach>
    </select>
    <input type="submit" value="Open">
    <input type="submit" id="back" value="Back">
    <input type=submit id="del" value="Delete"><br/>
</form>
<form method="post" action="filecreate.jsp">
    <input type="submit" id="create" value="Create">
</form>

<script>$(document).ready(function () {
    $("#selectedpath").hide();
    $("#previouspath").hide();
    $("#del").click(function () {
        $("#methodName").val("delete");
    });
    $("#create").click(function () {
        $("#methodName").val("post");
        $("#filepath").val($("#selectedpath").text());
    });
    $("#back").click(function () {
        $("#filepath").val($("#previouspath").text());
    });
    $("option").click(function () {
        var selected = $("#fileList").find("option:selected").text();
        $("#filepath").val($("#selectedpath").text() + selected);
    });
});</script>

</body>
</html>
