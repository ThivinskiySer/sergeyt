package lesson9;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Scanner;

@WebServlet(name = "testServlet", urlPatterns = {"/app"})
public class FileManager extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String methodName = req.getParameter("methodName");
        switch (methodName) {
            case "get":
                doGet(req, resp);
                break;
            case "delete":
                doDelete(req, resp);
                break;
            case "post":
                doPost(req, resp);
                break;
        }
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String filepath;
        String previouspath = "";

        if (req.getAttribute("filepath") == null) {
            filepath = req.getParameter("filepath");
        } else {
            filepath = req.getAttribute("filepath").toString();
        }

        if (filepath.length() == 3) {
            previouspath = filepath;
            req.setAttribute("filepath", filepath);
            req.setAttribute("previouspath", previouspath);
            File file = new File(filepath);
            String[] fileNamesArray = file.list();
            req.setAttribute("fileNamesArray", fileNamesArray);
            req.getSession().setAttribute("filepath", filepath);
            req.getRequestDispatcher("filemanager.jsp").forward(req, resp);
        }
        if ('\\' == filepath.charAt(filepath.length() - 1)) {
            previouspath = filepath.substring(0, filepath.length() - 1);
            previouspath = previouspath.substring(0, previouspath.lastIndexOf("\\")) + "\\";
        } else {
            previouspath = filepath.substring(0, filepath.lastIndexOf("\\")) + "\\";
            filepath = filepath.concat("\\");
        }
        req.setAttribute("filepath", filepath);
        req.setAttribute("previouspath", previouspath);
        File file = new File(filepath);
        String context = "";

        if (file.isFile()) {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext())
                context += scanner.nextLine() + "\r\n";
            scanner.close();

            filepath = filepath.substring(0, filepath.length() - 1);
            filepath = filepath.substring(0, previouspath.lastIndexOf("\\")) + "\\";
            req.setAttribute("filepath", filepath);
            req.getSession().setAttribute("filepath", filepath);
            req.setAttribute("context", context);
            req.getRequestDispatcher("filepresentation.jsp").forward(req, resp);
        } else {
            String[] fileNamesArray = file.list();
            req.setAttribute("fileNamesArray", fileNamesArray);
            req.getSession().setAttribute("filepath", filepath);
            req.getRequestDispatcher("filemanager.jsp").forward(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String filepath = req.getSession().getAttribute("filepath").toString() + req.getParameter("fileName");
        File file = new File(filepath);
        if (!file.exists()) {
            file.createNewFile();
        }
        try (PrintWriter out = new PrintWriter(filepath)) {
            out.print(req.getParameter("text"));
        }
        filepath = filepath.substring(0, filepath.lastIndexOf("\\")) + "\\";
        req.setAttribute("filepath", filepath);
        req.getSession().setAttribute("filepath", filepath);
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String filepath = req.getParameter("filepath");
        File file = new File(filepath);
        file.delete();
        filepath = filepath.substring(0, filepath.length() - 1);
        filepath = filepath.substring(0, filepath.lastIndexOf("\\")) + "\\";
        req.setAttribute("filepath", filepath);
        doGet(req, resp);
    }
}

