package ua.geekhub.thivinskiy.lesson2.vehicle_elements;


public interface ForceProvider {

    void start(String energyType);

    void move(ForceAcceptor forceAcceptor);

    void stop();
}
