package ua.geekhub.thivinskiy.lesson2.vehicle_elements;


public interface EnergyProvider {

    String supplyPower();

}
