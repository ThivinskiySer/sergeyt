package ua.geekhub.thivinskiy.lesson2.vehicle_elements;


public interface ForceAcceptor {

    void turn();
}
