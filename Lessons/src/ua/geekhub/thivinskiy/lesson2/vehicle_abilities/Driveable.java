package ua.geekhub.thivinskiy.lesson2.vehicle_abilities;


public interface Driveable {

    void accelerate();

    void brake();

    void turn();
}
