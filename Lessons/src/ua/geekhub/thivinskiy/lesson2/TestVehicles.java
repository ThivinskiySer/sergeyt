package ua.geekhub.thivinskiy.lesson2;

import ua.geekhub.thivinskiy.lesson2.vehicle_abilities.Driveable;
import ua.geekhub.thivinskiy.lesson2.vehicles.Boat;
import ua.geekhub.thivinskiy.lesson2.vehicles.SolarPoweredCar;

import java.util.Scanner;

public class TestVehicles {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Driveable vehicle = null;

        System.out.println("Select vehicle: 1 - boat, 2 - car");
        int userChoose = scanner.nextInt();
        switch (userChoose) {
            case 1:
                vehicle = new Boat();
                break;
            case 2:
                vehicle = new SolarPoweredCar();
                break;
            default:
                System.out.println("Wrong input information!");
                System.exit(-1);
        }
        while (true) {
            System.out.println("Select action: 1 - accelerate, 2 - turn, 3 - brake, 0 - finish program");
            userChoose = scanner.nextInt();
            switch (userChoose) {
                case 0:
                    System.exit(0);
                case 1:
                    vehicle.accelerate();
                    break;
                case 2:
                    vehicle.turn();
                    break;
                case 3:
                    vehicle.brake();
                    break;
                default:
                    System.out.println("Wrong input information!");
                    continue;
            }
        }
    }
}
