package ua.geekhub.thivinskiy.lesson2.vehicles;

import ua.geekhub.thivinskiy.lesson2.vehicle_elements.ForceAcceptor;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.ElectricEngine;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.SolarBattery;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.Wheel;

public class SolarPoweredCar extends Vehicle {

    public SolarPoweredCar() {
        forceProvider = new ElectricEngine();
        energyProvider = new SolarBattery();
        forceAcceptor = new Wheel[4];
        for (int i = 0; i < 4; i++) {
            forceAcceptor[i] = new Wheel();
        }
    }

    @Override
    public void accelerate() {
        super.accelerate();
        System.out.println("SolarPoweredCar started!");
    }

    @Override
    public void brake() {
        super.brake();
        System.out.println("SolarPoweredCar stopped!");
    }

    @Override
    public void turn() {
        forceAcceptor[0].turn();
        forceAcceptor[1].turn();
        System.out.println("SolarPoweredCar turned!");
    }
}
