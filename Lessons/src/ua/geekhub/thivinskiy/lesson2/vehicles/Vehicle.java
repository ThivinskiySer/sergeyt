package ua.geekhub.thivinskiy.lesson2.vehicles;

import ua.geekhub.thivinskiy.lesson2.vehicle_abilities.Driveable;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements.EnergyProvider;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements.ForceAcceptor;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements.ForceProvider;

public abstract class Vehicle implements Driveable {

    protected ForceProvider forceProvider;
    protected ForceAcceptor[] forceAcceptor;
    protected EnergyProvider energyProvider;

    @Override
    public void accelerate() {
        forceProvider.start(energyProvider.supplyPower());
        for (int i = 0; i < forceAcceptor.length; i++) {
            forceProvider.move(forceAcceptor[i]);
        }
    }

    @Override
    public void brake() {
        forceProvider.stop();
    }

    @Override
    public void turn() {
        forceAcceptor[0].turn();
    }
}
