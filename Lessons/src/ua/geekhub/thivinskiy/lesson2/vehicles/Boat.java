package ua.geekhub.thivinskiy.lesson2.vehicles;

import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.DieselEngine;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.GasTank;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.Propeller;

public class Boat extends Vehicle {

    public Boat() {
        forceProvider = new DieselEngine();
        energyProvider = new GasTank();
        forceAcceptor = new Propeller[]{new Propeller()};
    }

    @Override
    public void accelerate() {
        super.accelerate();
        System.out.println("Boat started!");
    }

    @Override
    public void brake() {
        super.brake();
        System.out.println("Boat stopped!");
    }

    @Override
    public void turn() {
        super.turn();
        System.out.println("Boat turned!");
    }
}
