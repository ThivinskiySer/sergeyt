package ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl;

import ua.geekhub.thivinskiy.lesson2.vehicle_elements.ForceAcceptor;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements.ForceProvider;

public class ElectricEngine implements ForceProvider {

    public void start(String energyType) {
        System.out.println(energyType + " received.");
        System.out.println("Starting electric engine...");
    }

    public void move(ForceAcceptor forceAcceptor) {
        System.out.println(forceAcceptor.getClass().getSimpleName() + " rotated.");
    }

    public void stop() {
        System.out.println("Stopping electric engine...");
    }
}
