package ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl;

import ua.geekhub.thivinskiy.lesson2.vehicle_elements.EnergyProvider;

public class SolarBattery implements EnergyProvider {

    public String supplyPower() {
        System.out.println("Supplying power...");
        return "Electricity";
    }


}
