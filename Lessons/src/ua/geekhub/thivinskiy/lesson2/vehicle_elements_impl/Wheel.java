package ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl;

import ua.geekhub.thivinskiy.lesson2.vehicle_elements.ForceAcceptor;

public class Wheel implements ForceAcceptor {

    public void turn() {
        System.out.println("Wheel turn...");
    }
}
