package ua.geekhub.thivinskiy.lesson3.task3;

public class ControlSystemCrashedException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Warning! Can not turn! Control system broke down!";
    }
}
