package ua.geekhub.thivinskiy.lesson3.task3;

public class OutOfFillException extends Exception {

    @Override
    public String getMessage() {
        return "Gas-tank in empty!";
    }
}
