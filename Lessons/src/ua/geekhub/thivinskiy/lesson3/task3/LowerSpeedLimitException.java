package ua.geekhub.thivinskiy.lesson3.task3;

public class LowerSpeedLimitException extends Exception {
    int speedLimit;

    public LowerSpeedLimitException(int speed) {
        speedLimit = speed;
    }

    @Override
    public String getMessage() {
        return "Minimum speed achieved! Current speed = " + speedLimit + " Car stopped!";
    }
}
