package ua.geekhub.thivinskiy.lesson3.task3;

public class OverflowTankException extends Exception {

    @Override
    public String getMessage() {
        return "The gas-tank is full!";
    }
}
