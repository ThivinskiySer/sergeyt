package ua.geekhub.thivinskiy.lesson3.task3;

public class UpperSpeedLimitException extends Exception {
    int speedLimit;

    public UpperSpeedLimitException(int speed) {
        speedLimit = speed;
    }

    @Override
    public String getMessage() {
        return "Maximum speed achieved! Current speed = " + speedLimit;
    }
}
