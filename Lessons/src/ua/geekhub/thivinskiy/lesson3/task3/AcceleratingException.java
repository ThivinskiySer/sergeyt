package ua.geekhub.thivinskiy.lesson3.task3;

public class AcceleratingException extends Exception {

    @Override
    public String getMessage() {
        return "Car should be started!";
    }
}
