package ua.geekhub.thivinskiy.lesson3.task3;

import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.DieselEngine;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.GasTank;
import ua.geekhub.thivinskiy.lesson2.vehicle_elements_impl.Wheel;
import ua.geekhub.thivinskiy.lesson2.vehicles.Vehicle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Car extends Vehicle {
    public int currentAmountFuel = 40;
    public int tankCapacity = 100;
    public int currentSpeed = 0;
    public int maxSpeed = 200;
    public int fuelConsumptionOn100km = 5;
    public boolean accelerated = false;

    public String userGuide() {
        StringBuilder stringBuilder = new StringBuilder(15);
        stringBuilder.append("This program emulate driving car. \n");
        stringBuilder.append("You can use list of methods: \n");
        stringBuilder.append("1 - accelerate car \n");
        stringBuilder.append("2 - stop car \n");
        stringBuilder.append("3 - increase speed by 10 km/h \n");
        stringBuilder.append("4 - decrease speed by 10 km/h \n");
        stringBuilder.append("5 - see current amount of fuel \n");
        stringBuilder.append("6 - see current speed \n");
        stringBuilder.append("7 - fill fuel by 10 liters \n");
        stringBuilder.append("8 - travel 100 kilometers \n");
        stringBuilder.append("9 - turn the car \n");
        stringBuilder.append("0 - exit from program \n");
        return stringBuilder.toString();
    }

    public Car() {
        forceProvider = new DieselEngine();
        energyProvider = new GasTank();
        forceAcceptor = new Wheel[4];
        for (int i = 0; i < 4; i++) {
            forceAcceptor[i] = new Wheel();
        }
    }

    @Override
    public void accelerate() {
        super.accelerate();
        accelerated = true;
        System.out.println("Car started!");
    }

    @Override
    public void brake() {
        super.brake();
        accelerated = false;
        System.out.println("Car stopped!");
    }

    @Override
    public void turn() {
        if ((int) (Math.random() * 10) == 0) {
            throw new ControlSystemCrashedException();
        } else {
            forceAcceptor[0].turn();
            forceAcceptor[1].turn();
            System.out.println("Car turned!");
        }
    }

    public void travel100km() throws OutOfFillException, AcceleratingException {
        if (!accelerated) {
            throw new AcceleratingException();
        } else if (currentAmountFuel > 0) {
            currentAmountFuel -= fuelConsumptionOn100km;
            System.out.println("You traveled 100 kilometers ");
        } else {
            throw new OutOfFillException();
        }
    }

    public void fillFuel10L() throws OverflowTankException {
        if (currentAmountFuel < tankCapacity) {
            currentAmountFuel += 10;
            System.out.println("filled fuel by 10 liters");
        } else {
            throw new OverflowTankException();
        }
    }

    public void turbo() throws AcceleratingException, UpperSpeedLimitException {
        if (!accelerated) {
            throw new AcceleratingException();
        } else if (currentSpeed <= maxSpeed - 50) {
            currentSpeed += 50;
            System.out.println("Speed increased by 50km/h");
        } else {
            throw new UpperSpeedLimitException(currentSpeed);
        }
    }

    public void increaseSpeedBy10() throws UpperSpeedLimitException, AcceleratingException {
        if (!accelerated) {
            throw new AcceleratingException();
        } else if (currentSpeed < maxSpeed) {
            currentSpeed += 10;
            System.out.println("Speed increased by 10km/h");
        } else {
            throw new UpperSpeedLimitException(currentSpeed);
        }
    }

    public void decreaseSpeedBy10() throws LowerSpeedLimitException, AcceleratingException {
        if (!accelerated) {
            throw new AcceleratingException();
        } else if (currentSpeed > 10) {
            currentSpeed -= 10;
            System.out.println("Speed decreased by 10km/h");
        } else {
            throw new LowerSpeedLimitException(currentSpeed);
        }
    }

    public int showCurrentAmountFuel() {
        System.out.println("Current amount of fuel is" + currentAmountFuel + "liters");
        return currentAmountFuel;
    }

    public int showCurrentSpeed() {
        System.out.println("Current speed is " + currentSpeed + "km/h");
        return currentSpeed;
    }

    public void controlPanel() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String userChoose = "";

        while (true) {
            try {
                userChoose = bufferedReader.readLine();
                if (userChoose.equals("?")) {
                    System.out.println(userGuide());
                    userChoose = bufferedReader.readLine();
                }
                switch (Integer.parseInt(userChoose)) {
                    case 0:
                        System.exit(0);
                    case 1:
                        accelerate();
                        break;
                    case 2:
                        brake();
                        break;
                    case 3:
                        increaseSpeedBy10();
                        break;
                    case 4:
                        decreaseSpeedBy10();
                        break;
                    case 5:
                        showCurrentAmountFuel();
                        break;
                    case 6:
                        showCurrentSpeed();
                        break;
                    case 7:
                        fillFuel10L();
                        break;
                    case 8:
                        travel100km();
                        break;
                    case 9:
                        turn();
                        break;
                    case 10:
                        turbo();
                        break;
                    default:
                        throw new IOException();
                }
            } catch (NumberFormatException e){
               System.out.println("Enter correct number of method!");
            } catch (IOException e) {
                System.out.println("Enter correct number of method");
            } catch (UpperSpeedLimitException e) {
                System.out.println(e.getMessage());
                System.out.println("Select another activity.");
            } catch (OverflowTankException e) {
                System.out.println(e.getMessage());
                System.out.println("Select another activity.");
            } catch (OutOfFillException e) {
                System.out.println(e.getMessage());
                brake();
            } catch (LowerSpeedLimitException e) {
                System.out.println(e.getMessage());
                brake();
            } catch (AcceleratingException e) {
                System.out.println(e.getMessage());
                System.out.println("Select accelerate() method");
            }
        }
    }

    public static void main(String[] args) {
        Car car = new Car();
        car.controlPanel();
    }
}
