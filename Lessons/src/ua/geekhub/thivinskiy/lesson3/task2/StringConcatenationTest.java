package ua.geekhub.thivinskiy.lesson3.task2;


public class StringConcatenationTest {

    public void testString(String someString) {
        String result = null;
        for (int i = 0; i < 20000; i++) {
            result += someString;
        }
    }

    public void testStringBuffer(String someString) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < 20000; i++) {
            stringBuffer.append(someString);
        }
    }

    public void testStringBuilder(String someString) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 20000; i++) {
            stringBuilder.append(someString);
        }
    }

    public static void main(String[] args) {
        StringConcatenationTest sct = new StringConcatenationTest();
        long startTime = System.currentTimeMillis();
        sct.testString("java");
        long resultTime = System.currentTimeMillis() - startTime;
        System.out.println("testString() was working " + resultTime);

        startTime = System.currentTimeMillis();
        sct.testStringBuffer("java");
        resultTime = System.currentTimeMillis() - startTime;
        System.out.println("testStringBuffer() was working " + resultTime);

        startTime = System.currentTimeMillis();
        sct.testStringBuilder("java");
        resultTime = System.currentTimeMillis() - startTime;
        System.out.println("testStringBuffer() was working " + resultTime);
    }

}
