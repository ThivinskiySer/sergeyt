package ua.geekhub.thivinskiy.lesson3.task1;

import java.util.Arrays;

public class Sorter {

    public Comparable[] sort(Comparable[] elements) {
        Comparable[] copyElements = Arrays.copyOf(elements, elements.length);
        Arrays.sort(copyElements);
        return copyElements;
    }

    public static void main(String[] args) {
        Book[] books = {new Book(1299), new Book(10), new Book(666), new Book(10)};
        Sorter sorter = new Sorter();
        Book[] sortedBooks = (Book[]) sorter.sort(books);
        for (Book book : sortedBooks) {
            System.out.println(book);
        }
        Person[] persons = {new Person("Lee", "Boynton"), new Person("Arthur", "Hoff"),
                new Person("Martin", "Buchholz"), new Person("Ulf", "Zibis")};
        Person[] sortedPersons = (Person[]) sorter.sort(persons);
        for (Person person : sortedPersons) {
            System.out.println(person);
        }
    }
}
