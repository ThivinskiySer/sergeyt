package ua.geekhub.thivinskiy.lesson3.task1;

public class Person implements Comparable<Person> {
    public String firstName;
    public String secondName;

    public Person(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    @Override
    public int compareTo(Person person) {
        if (person.firstName.length() - this.firstName.length() != 0) {
            return person.firstName.length() - this.firstName.length();
        } else {
            return person.secondName.length() - this.secondName.length();
        }
    }

    @Override
    public String toString() {
        return firstName + " " + secondName;
    }
}
