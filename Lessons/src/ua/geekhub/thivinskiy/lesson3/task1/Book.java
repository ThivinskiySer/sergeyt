package ua.geekhub.thivinskiy.lesson3.task1;

public class Book implements Comparable<Book> {
    private static int i = 1;
    private int id = i++;
    public int totalPages;

    public Book(int totalPages) {
        this.totalPages = totalPages;
    }

    @Override
    public int compareTo(Book anotherBook) {
        return anotherBook.totalPages - this.totalPages;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " " + id + " has " + this.totalPages + " pages";
    }
}
