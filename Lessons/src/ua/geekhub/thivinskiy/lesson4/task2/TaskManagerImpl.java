package ua.geekhub.thivinskiy.lesson4.task2;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TaskManagerImpl implements TaskManager {

    private SortedMap<Date, Task> tasksCollection = new TreeMap<>(new Comparator<Date>() {

        @Override
        public int compare(Date d1, Date d2) {
            return d1.compareTo(d2);
        }
    });
    private SimpleDateFormat formattedDate = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    @Override
    public void addTask(Date date, Task task) {
        tasksCollection.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        if (tasksCollection.containsKey(date)) {
            tasksCollection.remove(date);
        } else {
            System.out.println("Such date was not found");
        }
    }

    @Override
    public Collection<String> getCategories() {
        HashSet<String> categories = new HashSet<>();
        for (Map.Entry<Date, Task> entry : tasksCollection.entrySet()) {
            categories.add(entry.getValue().getCategory());
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        HashMap<String, List<Task>> tasksByCategory = new HashMap<>();
        String category = "";
        for (Map.Entry<Date, Task> entry : tasksCollection.entrySet()) {
            category = entry.getValue().getCategory();
            if (tasksByCategory.containsKey(category)) {
                tasksByCategory.get(category).add(entry.getValue());
            } else {
                tasksByCategory.put(category, new ArrayList<>());
                tasksByCategory.get(category).add(entry.getValue());
            }
        }
        return tasksByCategory;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        ArrayList<Task> tasks = new ArrayList<>();
        if (getCategories().contains(category)) {
            tasks.addAll(getTasksByCategories().get(category));
        } else {
            System.out.println("Such category was not found");
        }
        return tasks;
    }

    @Override
    public List<Task> getTasksForToday() {
        ArrayList<Task> todayTasks = new ArrayList<>();
        long oneDay = 24 * 60 * 60 * 1000;
        long today = new Date().getTime() / oneDay;
        Date beginToday = new Date(today * oneDay - TimeZone.getDefault().getRawOffset());
        Date endToday = new Date(beginToday.getTime() + oneDay);

        for (Map.Entry<Date, Task> entry : tasksCollection.entrySet()) {
            if (entry.getKey().after(beginToday) && entry.getKey().before(endToday)) {
                todayTasks.add(entry.getValue());
            }
        }
        return todayTasks;
    }

    public String userGuide() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("This program helps you to manage your tasks. \n");
        stringBuilder.append("You can use list of methods: \n");
        stringBuilder.append("1 - add task \n");
        stringBuilder.append("2 - remove task \n");
        stringBuilder.append("3 - print available categories of tasks \n");
        stringBuilder.append("4 - print tasks by categories \n");
        stringBuilder.append("5 - print tasks by specified category \n");
        stringBuilder.append("6 - print tasks scheduled for today \n");
        stringBuilder.append("0 - exit from program \n");
        return stringBuilder.toString();
    }

    public void userInterface() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String methodNumber = "";

        System.out.println("Enter ? for get help!");
        while (true) {
            try {
                System.out.println("Select needed action");
                methodNumber = bufferedReader.readLine();
                if (methodNumber.equals("?")) {
                    System.out.println(userGuide());
                    methodNumber = bufferedReader.readLine();
                }
                switch (Integer.parseInt(methodNumber)) {
                    case 0:
                        System.exit(0);
                    case 1:
                        System.out.println("Enter date in format: dd.MM.yyyy HH:mm");
                        String date = bufferedReader.readLine();
                        System.out.println("Enter category of task:");
                        String category = bufferedReader.readLine();
                        System.out.println("Enter description of task:");
                        String description = bufferedReader.readLine();
                        addTask(formattedDate.parse(date), new Task(category, description));
                        System.out.println("Task was added!");
                        break;
                    case 2:
                        System.out.println("Enter date in format dd.MM.yyyy HH:mm for remove task");
                        date = bufferedReader.readLine();
                        removeTask(formattedDate.parse(date));
                        System.out.println("Task was removed!");
                        break;
                    case 3:
                        System.out.print("List of categories: ");
                        System.out.println(getCategories());
                        break;
                    case 4:
                        Iterator iterator = getTasksByCategories().entrySet().iterator();
                        while (iterator.hasNext()) {
                            Map.Entry pairs = (Map.Entry) iterator.next();
                            System.out.println("Category: " + pairs.getKey() + " Tasks: " + pairs.getValue());
                        }
                        break;
                    case 5:
                        System.out.println("Enter the name of the category");
                        category = bufferedReader.readLine();
                        System.out.print("Category " + category + " includes following tasks: ");
                        System.out.println(getTasksByCategory(category));
                        break;
                    case 6:
                        System.out.print("Following tasks scheduled for today: ");
                        getTasksForToday();
                        break;
                    default:
                        throw new IOException();
                }
            } catch (NumberFormatException | IOException e) {
                System.out.println("Enter correct number of method!");
            } catch (ParseException e) {
                System.out.println("Enter correct date!");
            }
        }
    }

    public static void main(String[] args) {
        new TaskManagerImpl().userInterface();
    }
}
