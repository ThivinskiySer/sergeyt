package ua.geekhub.thivinskiy.lesson4.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class SetOperationsImpl implements SetOperations {

    @Override
    public boolean equals(Set a, Set b) {
        return a.containsAll(b);
    }

    @Override
    public Set union(Set a, Set b) {
        Set united = new HashSet(a);
        united.addAll(b);
        return united;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set subtracted = new HashSet(a);
        subtracted.removeAll(b);
        return subtracted;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set intersected = new HashSet(a);
        intersected.retainAll(b);
        return intersected;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set symmetricSubtracted = union(a, b);
        symmetricSubtracted.remove(intersect(a, b));
        return symmetricSubtracted;
    }

    public Set randomSet() {
        Set randomSet = new HashSet();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            randomSet.add(random.nextInt(30));
        }
        return randomSet;
    }

    public String userGuide() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("This program provides methods for manipulate with two sets. \n");
        stringBuilder.append("You can use list of methods: \n");
        stringBuilder.append("1 - create random sets of numbers \n");
        stringBuilder.append("2 - create sets by yourself \n");
        stringBuilder.append("3 - print sets \n");
        stringBuilder.append("4 - compare sets \n");
        stringBuilder.append("5 - unite sets \n");
        stringBuilder.append("6 - subtract sets \n");
        stringBuilder.append("7 - symmetric subtract sets \n");
        return stringBuilder.toString();
    }

    public void userInterface() {
        Set a = new HashSet();
        Set b = new HashSet();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String userChoose = "";

        System.out.println("Enter ? for get help!");
        while (true) {
            try {
                System.out.println("Select needed action");
                userChoose = bufferedReader.readLine();
                if (userChoose.equals("?")) {
                    System.out.println(userGuide());
                    userChoose = bufferedReader.readLine();
                }
                switch (Integer.parseInt(userChoose)) {
                    case 0:
                        System.exit(0);
                    case 1:
                        a.addAll(randomSet());
                        b.addAll(randomSet());
                        System.out.println("Sets was created!");
                        break;
                    case 2:
                        System.out.println("Enter the first set of elements, separated by a space");
                        userChoose = bufferedReader.readLine();
                        String[] s = userChoose.split(" ");
                        for (int i = 0; i < s.length; i++) {
                            a.add(Integer.parseInt(s[i]));
                        }
                        System.out.println("Enter the second set of elements, separated by a space");
                        userChoose = bufferedReader.readLine();
                        String[] s1 = userChoose.split(" ");
                        for (int i = 0; i < s1.length; i++) {
                            b.add(Integer.parseInt(s1[i]));
                        }
                        break;
                    case 3:
                        System.out.println("First set: " + a);
                        System.out.println("Second set: " + b);
                        break;
                    case 4:
                        if (equals(a, b)) {
                            System.out.println("Sets the same.");
                        } else {
                            System.out.println("Sets the different.");
                        }
                        break;
                    case 5:
                        System.out.println("Result of unification sets:");
                        System.out.println(union(a, b));
                        break;
                    case 6:
                        System.out.println("Result of subtraction sets:");
                        System.out.println(subtract(a, b));
                        break;
                    case 7:
                        System.out.println("Result of intersection sets:");
                        System.out.println(intersect(a, b));
                        break;
                    case 8:
                        System.out.println("Result of symmetric subtraction sets:");
                        System.out.println(symmetricSubtract(a, b));
                        break;
                    default:
                        throw new IOException();
                }
            } catch (NumberFormatException e) {
                System.out.println("Enter correct number of method!");
            } catch (IOException e) {
                System.out.println("Enter correct number of method!");
            }
        }
    }

    public static void main(String[] args) {
        SetOperationsImpl s = new SetOperationsImpl();
        s.userInterface();
    }
}

