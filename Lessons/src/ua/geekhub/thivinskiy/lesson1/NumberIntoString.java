package ua.geekhub.thivinskiy.lesson1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


/**
 * Created by ThivinskiySergey on 18.10.2014.
 */
public class NumberIntoString {

    public void chooseMethod(){

        int number = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 0 to get help");
        int numberOfMethod = scanner.nextInt();

        if (numberOfMethod==0){
            System.out.println("1 - translate by switch\n"
                             + "2 - translate by array\n"
                             + "3 - translate by ArrayList\n"
                             + "-1 - for exit from program");
            numberOfMethod = scanner.nextInt();
        }
        switch (numberOfMethod) {
            case -1:
                System.out.println("Thank you for use my program!");
                return;
            case 1:
                System.out.println("Enter the numeral");
                number = scanner.nextInt();
                changeBySwitch(number);
                break;
            case 2:
                System.out.println("Enter the numeral");
                number = scanner.nextInt();
                changeByArray(number);
                break;
            case 3:
                System.out.println("Enter the numeral");
                number = scanner.nextInt();
                changeByArrayList(number);
                break;
            default:
                System.out.println("Read help");
                break;
        }
    }

    public void changeBySwitch(int number){
        if (number>9){
            System.out.println("Enter numeral >=0 and <=9");
            return;
        }
        switch (number){
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
            case 3:
                System.out.println("Three");
                break;
            case 4:
                System.out.println("Four");
                break;
            case 5:
                System.out.println("Five");
                break;
            case 6:
                System.out.println("Six");
                break;
            case 7:
                System.out.println("Seven");
                break;
            case 8:
                System.out.println("Eight");
                break;
            case 9:
                System.out.println("Nine");
                break;
            case 0:
                System.out.println("Zero");
                break;
        }
    }

    public void changeByArray(int number){
        if (number>9){
            System.out.println("Enter numeral >=0 and <=9");
            return;
        }
        String [] numbers = {"Zero","One","Two","Three","Four","Five","Six","Seven","Eight","Nine"};
        System.out.println(numbers[number]);
    }

    public void changeByArrayList(int number){
        if (number>9){
            System.out.println("Enter numeral >=0 and <=9");
            return;
        }
        ArrayList numbers = new ArrayList();
        Collections.addAll(numbers, "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine");
        System.out.println(numbers.get(number));
    }

    public static void main(String[] args) {
         new NumberIntoString().chooseMethod();


    }
}
