package ua.geekhub.thivinskiy.lesson1;

import java.util.Scanner;

/**
 * Created by ThivinskiySergey on 18.10.2014.
 */
public class FirstLesson {

    public void mathFunctions() {
        System.out.println("Enter 0 to get help");
        Scanner scanner = new Scanner(System.in);
        int number = 0;

        loop:
        while (scanner.hasNext()) {
            int numberOfMethod = scanner.nextInt();
            switch (numberOfMethod) {
                case 0:
                    System.out.println("1 - call function of factorial\n"
                                     + "2 - call function of fibonacci\n"
                                     + "-1 - for exit from program");
                    break;
                case -1:
                    System.out.println("Thank you for use my program!");
                    return;
                case 1:
                    System.out.println("Enter the number to get its factorial");
                    number = scanner.nextInt();
                    factorial(number);
                    break;
                case 2:
                    System.out.println("Enter the size of the Fibonacci sequence");
                    number = scanner.nextInt();
                    fibonacci(number);
                    break;
                default:
                    System.out.println("Enter 0 to get help");
                    break;
            }
        }
    }

    private void factorial(int number) {
        double n = number;
        double result = 1.0;

        if (n == 0 || n == 1) System.out.println("Factorial = 1");
        else {
            for (double i = n; i > 1; i--) {
                result *= i;
            }
            System.out.println("Factorial = " + result);
        }
    }

    private void fibonacci(int number) {
        int n = number;
        long fibSequence[] = new long[n];

        for (int i = 0; i < fibSequence.length; i++) {
            if (i < 2) {
                fibSequence[i] = i;
            } else {
                fibSequence[i] = fibSequence[i - 1] + fibSequence[i - 2];
            }
        }
        System.out.print("Fibonacci sequence: ");
        for (int i = 0; i < fibSequence.length; i++) {
            System.out.print(fibSequence[i]);
            System.out.print(i==fibSequence.length-1?".":", ");
        }
        System.out.println();
    }

    public static void main(String args[]) {
        FirstLesson fs = new FirstLesson();
        fs.mathFunctions();
    }
}
