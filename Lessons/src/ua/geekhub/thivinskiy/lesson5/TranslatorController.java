package ua.geekhub.thivinskiy.lesson5;

import ua.geekhub.thivinskiy.lesson5.source.SourceLoader;
import ua.geekhub.thivinskiy.lesson5.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) {
            //      So, the only way to stop the application is to do that manually or type "exit"
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);

                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);

                command = scanner.next();
            } catch (Exception e) {
                System.out.println("Enter another path to translation");
                command = scanner.next();
            }
        }
    }
}
