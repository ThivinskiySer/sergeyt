package ua.geekhub.thivinskiy.lesson5.source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader() {
        sourceProviders.add(new FileSourceProvider());
        sourceProviders.add(new URLSourceProvider());
    }

    public String loadSource(String pathToSource) throws IOException {
        String result;
        if (pathToSource.substring(0, 3).equals("htt") ||
                pathToSource.substring(0, 3).equals("ftp")) {
            result = sourceProviders.get(1).load(pathToSource);
        } else {
            result = sourceProviders.get(0).load(pathToSource);
        }
        return result;
    }
}
