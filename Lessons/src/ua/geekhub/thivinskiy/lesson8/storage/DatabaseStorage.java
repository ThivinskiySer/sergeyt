package ua.geekhub.thivinskiy.lesson8.storage;


import ua.geekhub.thivinskiy.lesson8.objects.Entity;
import ua.geekhub.thivinskiy.lesson8.objects.Ignore;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Implementation of {@link org.geekhub.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link org.geekhub.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            int rowsAffected = statement.executeUpdate(sql);
            return rowsAffected > 0;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        String sql;
        if (entity.isNew()) {
            StringBuffer fieldNames = new StringBuffer("(");
            StringBuffer fieldValues = new StringBuffer("(");
            for (String fieldName : data.keySet()) {
                fieldNames.append(fieldName);
                fieldNames.append(",");

                fieldValues.append("\"");
                fieldValues.append(data.get(fieldName));
                fieldValues.append("\",");
            }
            fieldNames.deleteCharAt(fieldNames.length() - 1);
            fieldValues.deleteCharAt(fieldValues.length() - 1);
            fieldNames.append(")");
            fieldValues.append(")");

            sql = "INSERT INTO " + entity.getClass().getSimpleName() + fieldNames.toString()
                    + " VALUES " + fieldValues.toString();
        } else {
            StringBuilder fieldsToUpdate = new StringBuilder();
            for (String fieldName : data.keySet()) {
                fieldsToUpdate.append(fieldName);
                fieldsToUpdate.append("=\"");
                fieldsToUpdate.append(data.get(fieldName));
                fieldsToUpdate.append("\",");
            }
            fieldsToUpdate.deleteCharAt(fieldsToUpdate.length() - 1);

            sql = "UPDATE " + entity.getClass().getSimpleName() + " SET " + fieldsToUpdate.toString()
                    + " WHERE id = " + entity.getId();
        }

        try (Statement statement = connection.createStatement()) {
            int affectedRows = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

            if (affectedRows == 0) {
                throw new SQLException("Save-method failed, no rows affected.");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    entity.setId(generatedKeys.getInt(1));
                }
            }
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> entityMap = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            Annotation[] annotations = field.getDeclaredAnnotations();
            if (annotations.length > 0) {
                for (Annotation annotation : annotations) {
                    if (!annotation.annotationType().equals(Ignore.class)) {
                        if (field.getType().equals(Boolean.class)) {
                            entityMap.put(field.getName(), (boolean) field.get(entity) ? 1 : 0);
                        } else {
                            entityMap.put(field.getName(), field.get(entity));
                        }
                    }
                }
            } else {
                if (field.getType().equals(Boolean.class)) {
                    entityMap.put(field.getName(), (boolean) field.get(entity) ? 1 : 0);
                } else {
                    entityMap.put(field.getName(), field.get(entity));
                }
            }
            field.setAccessible(false);
        }
        return entityMap;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        T instance;
        List<T> instancesList = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();

        while (resultset.next()) {
            instance = clazz.newInstance();
            for (Field field : fields) {
                field.setAccessible(true);
                Annotation[] annotations = field.getDeclaredAnnotations();
                if (annotations.length > 0) {
                    for (Annotation annotation : annotations) {
                        if (!annotation.annotationType().equals(Ignore.class)) {
                            field.set(instance, resultset.getObject(field.getName()));
                        }
                    }
                } else {
                    field.set(instance, resultset.getObject(field.getName()));
                }
                field.setAccessible(false);
            }
            instancesList.add(instance);
        }
        return instancesList;
    }
}
