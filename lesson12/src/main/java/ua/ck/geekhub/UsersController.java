package ua.ck.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UsersController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Hello world!");
        return "redirect:users";
    }

    @RequestMapping(value = "/users", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String user(ModelMap model) {
        model.addAttribute("users", userService.getUsers());
        return "users";
    }

    @RequestMapping(value = "/users", method = {RequestMethod.POST, RequestMethod.HEAD})
    public String createUser(
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email,
            @RequestParam String groupName
    ) {
        userService.createUser(firstName, lastName, email, groupName);
        return "redirect:users";
    }

    @RequestMapping(value = "/deleteUser", method = {RequestMethod.POST, RequestMethod.HEAD})
    public String deleteUser(@RequestParam int idUser) {
        userService.deleteUser(userService.getUser(idUser));
        return "redirect:users";
    }

    @RequestMapping(value = "/editUser", method = {RequestMethod.POST, RequestMethod.HEAD})
    public String editUser(
            @RequestParam int idUser,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email,
            @RequestParam String groupName
    ) {
        userService.editUser(idUser, firstName, lastName, email, groupName);
        return "redirect:users";
    }
}