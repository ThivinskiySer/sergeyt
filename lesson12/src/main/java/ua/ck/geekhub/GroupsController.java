package ua.ck.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GroupsController {

    @Autowired
    GroupService groupService;

    @RequestMapping(value = "/groups", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String groups(ModelMap model) {
        model.addAttribute("groups", groupService.getGroups());
        return "groups";
    }

    @RequestMapping(value = "/groups", method = {RequestMethod.POST, RequestMethod.HEAD})
    public String createGroup(@RequestParam String groupName) {
        groupService.createGroup(groupName);
        return "redirect:groups";
    }

    @RequestMapping(value = "/editGroup", method = {RequestMethod.POST, RequestMethod.HEAD})
    public String editGroup(
            @RequestParam int idGroup,
            @RequestParam String groupName

    ) {
        groupService.editGroup(idGroup, groupName);
        return "redirect:groups";
    }

    @RequestMapping(value = "/deleteGroup", method = {RequestMethod.POST, RequestMethod.HEAD})
    public String deleteGroup(@RequestParam int idGroup) {
        groupService.deleteGroup(groupService.getGroup(idGroup));
        return "redirect:groups";
    }
}
