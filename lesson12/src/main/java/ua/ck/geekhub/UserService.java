package ua.ck.geekhub;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vladimirb
 * @since 3/11/14
 */
@Repository
@Transactional
public class UserService {

    @Autowired
    SessionFactory sessionFactory;

    public void saveUser(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    public User getUser(Integer id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    public List<User> getUsers() {
        return sessionFactory.getCurrentSession()
                .createCriteria(User.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public void deleteUser(User user) {
        deleteUserInGroup(user);
        sessionFactory.getCurrentSession().delete(user);
    }

    public void createUser(String firstName, String lastName, String email, String groupName) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        Group group = (Group) sessionFactory.getCurrentSession().createCriteria(Group.class)
                .add(Restrictions.eq("name", groupName)).uniqueResult();
        if (group == null) {
            group = createNewGroup(user, groupName);
        } else {
            group = editGroup(user, group);
        }
        sessionFactory.getCurrentSession().saveOrUpdate(group);
        user.setGroup(group);
        saveUser(user);
    }

    public void editUser(int idUser, String firstName, String lastName, String email, String groupName) {
        User user = getUser(idUser);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        Group group = (Group) sessionFactory.getCurrentSession().createCriteria(Group.class)
                .add(Restrictions.eq("name", groupName)).uniqueResult();
        if (user.getGroup() != null) {
            user.getGroup().getUsers().remove(user);
        }
        if (group == null) {
            group = createNewGroup(user, groupName);
        } else {
            group = editGroup(user, group);
        }
        sessionFactory.getCurrentSession().saveOrUpdate(group);
        user.setGroup(group);
        saveUser(user);
    }

    private Group createNewGroup(User user, String groupName) {
        List<User> users = new ArrayList<>();
        Group group = new Group();
        group.setName(groupName);
        users.add(user);
        group.setUsers(users);
        return group;
    }

    private Group editGroup(User user, Group group) {
        List<User> users = group.getUsers();
        users.add(user);
        group.setUsers(users);
        return group;
    }

    private void deleteUserInGroup(User user) {
        Group group = user.getGroup();
        if (group != null) {
            group.getUsers().remove(user);
            sessionFactory.getCurrentSession().saveOrUpdate(group);
        }
    }
}
