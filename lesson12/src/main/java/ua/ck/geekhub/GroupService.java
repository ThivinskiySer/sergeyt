package ua.ck.geekhub;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class GroupService {

    @Autowired
    SessionFactory sessionFactory;

    public void saveGroup(Group group) {
        sessionFactory.getCurrentSession().saveOrUpdate(group);
    }

    public Group getGroup(Integer id) {
        return (Group) sessionFactory.getCurrentSession().get(Group.class, id);
    }

    public List<Group> getGroups() {
        return sessionFactory.getCurrentSession()
                .createCriteria(Group.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public void deleteGroup(Group group) {
        List<User> users = group.getUsers();
        for (User user : users) {
            user.setGroup(null);
            sessionFactory.getCurrentSession().saveOrUpdate(user);
        }
        sessionFactory.getCurrentSession().delete(group);
    }

    public void createGroup(String groupName) {
        Group group = new Group();
        if (isNotExist(groupName)) {
            group.setName(groupName);
            saveGroup(group);
        }
    }

    public void editGroup(int idGroup, String groupName) {
        Group group = getGroup(idGroup);
        if (isNotExist(groupName)) {
            group.setName(groupName);
            saveGroup(group);
        }
    }

    private boolean isNotExist(String groupName) {
        return sessionFactory.getCurrentSession().createCriteria(Group.class)
                .add(Restrictions.eq("name", groupName)).list().isEmpty();
    }
}
