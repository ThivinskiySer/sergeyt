<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <script>
        function Delete(ths) {
            var tr = ths.parentNode.parentNode;
            document.getElementById("idUser").value = tr.getElementsByTagName("td")[0].innerHTML;
        }

        function Edit(ths) {
            var tr = ths.parentNode.parentNode;
            document.getElementById("idUser").value = tr.getElementsByTagName("td")[0].innerHTML;
            document.getElementById("firstName").value = tr.getElementsByTagName("td")[1]
                    .childNodes[0].value;
            document.getElementById("lastName").value = tr.getElementsByTagName("td")[2]
                    .childNodes[0].value;
            document.getElementById("email").value = tr.getElementsByTagName("td")[3]
                    .childNodes[0].value;
            document.getElementById("groupName").value = tr.getElementsByTagName("td")[4]
                    .childNodes[0].value;
        }
    </script>
</head>

<body>
<h1>Users</h1>

<form action="${pageContext.request.contextPath}/users" method="post">
    <input name="firstName" placeholder="First Name">
    <input name="lastName" placeholder="Last Name">
    <input name="email" type="email" placeholder="Email">
    <input name="groupName" placeholder="Group">
    <input type="submit" value="Submit">
</form>

<form action="${pageContext.request.contextPath}/deleteUser" method="post">
    <input type="hidden" id="idUser" name="idUser">
    <input type="hidden" id="firstName" name="firstName">
    <input type="hidden" id="lastName" name="lastName">
    <input type="hidden" id="email" name="email">
    <input type="hidden" id="groupName" name="groupName">

    <table border="1">
        <tr>
            <td>ID</td>
            <td>FIRST NAME</td>
            <td>LAST NAME</td>
            <td>EMAIL</td>
            <td>GROUP</td>
        </tr>

        <c:forEach var="user" items="${users}">
            <tr>
                <td>${user.id}</td>
                <td><input value="${user.firstName}"></td>
                <td><input value="${user.lastName}"></td>
                <td><input type="email" value="${user.email}"></td>
                <td><input value="${user.group.name}"></td>
                <td><input type="submit" value="Delete" onclick="Delete(this)"></td>
                <td>
                    <button type="submit" formaction="${pageContext.request.contextPath}/editUser"
                            onclick="Edit(this)">Edit
                    </button>
                </td>
            </tr>
        </c:forEach>
    </table>
    <button type="submit" formmethod="get" formaction="${pageContext.request.contextPath}/groups">Groups</button>
</form>

</body>
</html>