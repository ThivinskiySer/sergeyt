<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title></title>
    <script>
        function Delete(ths) {
            var tr = ths.parentNode.parentNode;
            document.getElementById("idGroup").value = tr.getElementsByTagName("td")[0].innerHTML;
        }

        function Edit(ths) {
            var tr = ths.parentNode.parentNode;
            document.getElementById("idGroup").value = tr.getElementsByTagName("td")[0].innerHTML;
            document.getElementById("groupName").value = tr.getElementsByTagName("td")[1]
                    .childNodes[0].value;
        }
    </script>
</head>
<body>
<h1>Groups</h1>

<form action="${pageContext.request.contextPath}/groups" method="post">
    <input name="groupName" placeholder="Group Name">
    <input type="submit" value="Submit">
</form>

<form action="${pageContext.request.contextPath}/editGroup" method="post">
    <input type="hidden" id="idGroup" name="idGroup">
    <input type="hidden" id="groupName" name="groupName">
    <table border="1">
        <tr>
            <td>ID</td>
            <td>NAME</td>
        </tr>
        <c:forEach var="group" items="${groups}">
            <tr>
                <td>${group.id}</td>
                <td><input value="${group.name}"></td>
                <td><input type="submit" value="Edit" onclick="Edit(this)"></td>
                <td>
                    <button type="submit" formaction="${pageContext.request.contextPath}/deleteGroup"
                            onclick="Delete(this)">Delete
                    </button>
                </td>
            </tr>
        </c:forEach>
    </table>
    <button type="submit" formmethod="get" formaction="${pageContext.request.contextPath}/users">Users</button>
</form>

</body>
</html>
